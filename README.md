coinwidget.com
==============

*The Bitcoin and Litecoin Donation Button*

Visit http://coinwidget.com/ or [b.agilob.net](http://b.agilob.net/bitcoin-litecoin-dogecoin-donation-widget-on-webpage) for full documentation, demo, and a link code wizard.

CoinWidget was created by http://scotty.cc (hire him for your next project!), modified by http://b.agilob.net 

Released under the Open Source **MIT License** (see **LICENSE** file for details).

Please help keep this project alive! Tell someone about this widget! 


Example installation
==============
A. Grab the latest version from GitLab: `git clone git@gitlab.com:agilob/BTC-LTC-DOGE-donation-widget.git`

B. Open **widget/coin.js** and find:

	source: 'https://agilob.net/widget/'

C. Change the URL portion of this line to your own server/path, my server no longer supports public requests.

D. (optional) Open **lookup.php** and consider implementing a caching method based on your own style and preference.


Example Code
==============

See [blog post](http://b.agilob.net/bitcoin-litecoin-dogecoin-donation-widget-on-webpage) for examples.

The complete list of the options and acceptable values can be found on http://coinwidget.com/.

You can also use the wizard on [my blog](http://b.agilob.net/bitcoin-litecoin-dogecoin-donation-widget-on-webpage) to generate linking codes, just be sure to change: `<script src="https://agilob.net/widget/coin.js"></script>` to use your own hosted copy of coin.js.

*Enjoy!*
[![piwik](https://agilob.net/piwik/piwik.php?idsite=5&rec=1)](https://agilob.net/)