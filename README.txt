coinwidget.com
==============

*The Bitcoin and Litecoin Donation Button*

Visit http://coinwidget.com/ or http://code.agilob.net/bitcoin-litecoin-dogecoin-donation-widget-on-webpage/ for full documentation, demo, and a link code wizard.

CoinWidget was created by http://scotty.cc (hire him for your next project!), modified by http://b.agilob.net 

Released under the Open Source **MIT License** (see **LICENSE** file for details).

Please help keep this project alive! Tell someone about this widget! 


Installation
==============
A. Grab the latest version from GitHub: https://github.com/agilob/coinwidget.com

B. Open **widget/coin.js** and find:

	source: 'http://agilob.net/widget/'

C. Change the URL portion of this line to your own server/path.

D. (optional) Open **lookup.php** and consider implementing a caching method based on your own style and preference.


Example Code
==============

See page http://code.agilob.net/bitcoin-litecoin-dogecoin-donation-widget-on-webpage/ for examples.

The complete list of the options and acceptable values can be found on http://coinwidget.com/.

You can also use the wizard on http://code.agilob.net/bitcoin-litecoin-dogecoin-donation-widget-on-webpage/ to generate linking codes, just be sure to change: `<script src="http://agilob.net/widget/coin.js"></script>` to use your own hosted copy of coin.js.

*Enjoy! @scottycc and @agilob*


==============
*If you run into a bug, or a styling issue, or if you need help or have an idea please e-mail:
coinwidget.com@gmail.com*
or
info@agilob.net
